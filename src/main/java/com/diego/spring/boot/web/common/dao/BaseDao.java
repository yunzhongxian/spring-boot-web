package com.diego.spring.boot.web.common.dao;

/**
 * @author Manfred
 *
 */
public interface BaseDao<T> {
	
	public T get(int id);
	
	public void add(T t);
	
	public int update(T t);
	
	public int delete(int id);
}
