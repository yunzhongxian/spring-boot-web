package com.diego.spring.boot.web.common.utils;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * @author Manfred
 *
 */
public class FileUtil {
	public final static Map<String, String> FILE_TYPE_MAP = new HashMap<String, String>();
	static {
		FILE_TYPE_MAP.put("jpg", "FFD8FF"); // JPEG (jpg)
		FILE_TYPE_MAP.put("png", "89504E47"); // PNG (png)
		FILE_TYPE_MAP.put("gif", "47494638"); // GIF (gif)
		FILE_TYPE_MAP.put("tif", "49492A00"); // TIFF (tif)
		FILE_TYPE_MAP.put("bmp", "424D"); // Windows Bitmap (bmp)
		FILE_TYPE_MAP.put("dwg", "41433130"); // CAD (dwg)
		FILE_TYPE_MAP.put("html", "68746D6C3E"); // HTML (html)
		FILE_TYPE_MAP.put("rtf", "7B5C727466"); // Rich Text Format (rtf)
		FILE_TYPE_MAP.put("xml", "3C3F786D6C");
		FILE_TYPE_MAP.put("zip", "504B0304");
		FILE_TYPE_MAP.put("rar", "52617221");
		FILE_TYPE_MAP.put("psd", "38425053"); // Photoshop (psd)
		FILE_TYPE_MAP.put("eml", "44656C69766572792D646174653A"); // Email
		// [thorough
		// only]
		// (eml)
		FILE_TYPE_MAP.put("dbx", "CFAD12FEC5FD746F"); // Outlook Express (dbx)
		FILE_TYPE_MAP.put("pst", "2142444E"); // Outlook (pst)
		FILE_TYPE_MAP.put("xls", "D0CF11E0"); // MS Word
		FILE_TYPE_MAP.put("doc", "D0CF11E0"); // MS Excel ע�⣺word ��
		// excel���ļ�ͷһ��
		FILE_TYPE_MAP.put("mdb", "5374616E64617264204A"); // MS Access (mdb)
		FILE_TYPE_MAP.put("wpd", "FF575043"); // WordPerfect (wpd)
		FILE_TYPE_MAP.put("eps", "252150532D41646F6265");
		FILE_TYPE_MAP.put("ps", "252150532D41646F6265");
		FILE_TYPE_MAP.put("pdf", "255044462D312E"); // Adobe Acrobat (pdf)
		FILE_TYPE_MAP.put("qdf", "AC9EBD8F"); // Quicken (qdf)
		FILE_TYPE_MAP.put("pwl", "E3828596"); // Windows Password (pwl)
		FILE_TYPE_MAP.put("wav", "57415645"); // Wave (wav)
		FILE_TYPE_MAP.put("avi", "41564920");
		FILE_TYPE_MAP.put("ram", "2E7261FD"); // Real Audio (ram)
		FILE_TYPE_MAP.put("rm", "2E524D46"); // Real Media (rm)
		FILE_TYPE_MAP.put("mpg", "000001BA"); //    
		FILE_TYPE_MAP.put("mov", "6D6F6F76"); // Quicktime (mov)
		FILE_TYPE_MAP.put("asf", "3026B2758E66CF11"); // Windows Media (asf)
		FILE_TYPE_MAP.put("mid", "4D546864"); // MIDI (mid)
	}

	/**
	 * ��ȡ�ļ�����,����ͼƬ,����ʽ���������õ�,�򷵻�null]
	 * </p>
	 * 
	 * @param file
	 * @return fileType
	 */
	public final static String getFileContentType(File file) {
		String filetype = "";
		byte[] b = new byte[50];
		try {
			InputStream is = new FileInputStream(file);
			is.read(b);
			filetype = getFileTypeByStream(b);
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filetype;
	}

	public final static String getFileTypeByStream(byte[] b) {
		String filetypeHex = String.valueOf(getFileHexString(b));
		Iterator<Entry<String, String>> entryiterator = FILE_TYPE_MAP
				.entrySet().iterator();
		while (entryiterator.hasNext()) {
			Entry<String, String> entry = entryiterator.next();
			String fileTypeHexValue = entry.getValue();
			if (filetypeHex.toUpperCase().startsWith(fileTypeHexValue)) {
				return entry.getKey();
			}
		}
		return null;
	}

	public final static String getFileHexString(byte[] b) {
		StringBuilder stringBuilder = new StringBuilder();
		if (b == null || b.length <= 0) {
			return null;
		}
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	/**
	 * �����ı��ļ�
	 * 
	 * @param file
	 * @param content
	 */
	public static void saveTxt(File file, String content) {
		try {
			if(!file.getParentFile().exists()){
				file.getParentFile().mkdirs();
			}			
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			out.write(content);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * �����ı��ļ�
	 * 
	 * @param file
	 * @param content
	 */
	public static void saveTxt(File file, String content,String charset) {
		try {
			if(!file.getParentFile().exists()){
				file.getParentFile().mkdirs();
			}			
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),charset));
			out.write(content);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * �����ı��ļ�
	 * 
	 * @param file
	 * @param content
	 */
	public static void saveTxt(File file, byte[] content) {
		try {
			if(!file.getParentFile().exists()){
				file.getParentFile().mkdirs();
			}			
			FileOutputStream out = new FileOutputStream(file);
			out.write(content);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * ��ȡ�ļ��ļ�
	 * 
	 * @param file
	 * @return
	 */
	public static String readTxt(File file) {
		StringBuffer strBuf = new StringBuffer();
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = in.readLine()) != null) {
				strBuf.append(line + "\r\n");
			}
			in.close();
			return strBuf.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * ��ȡ�ļ��ļ�
	 * 
	 * @param file
	 * @return
	 */
	public static String readTxt(File file,String charSet) {
		StringBuffer strBuf = new StringBuffer();
		try {
			InputStreamReader isr = new InputStreamReader(new FileInputStream(file),charSet);
			BufferedReader in = new BufferedReader(isr);
			String line = null;
			while ((line = in.readLine()) != null) {
				strBuf.append(line + "\r\n");
			}
			in.close();
			return strBuf.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * ���ֽ�����ȡString
	 * 
	 * @param file
	 * @return
	 */
	public static String readString(InputStream inputStream,String charSet) {
		StringBuffer strBuf = new StringBuffer();
		try {
			InputStreamReader isr = new InputStreamReader(inputStream,charSet);
			BufferedReader in = new BufferedReader(isr);
			String line = null;
			while ((line = in.readLine()) != null) {
				strBuf.append(line + "\r\n");
			}
			in.close();
			return strBuf.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * ���ֽ�����ȡString
	 * 
	 * @param file
	 * @return
	 */
	public static String readString(InputStream inputStream) {
		StringBuffer strBuf = new StringBuffer();
		try {
			InputStreamReader isr = new InputStreamReader(inputStream);
			BufferedReader in = new BufferedReader(isr);
			String line = null;
			while ((line = in.readLine()) != null) {
				strBuf.append(line + "\r\n");
			}
			in.close();
			return strBuf.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * ��ȡ�ļ��ļ�
	 * 
	 * @param file
	 * @return
	 */
	public static List<String> readLineToList(File file) {
		ArrayList<String> strLines=new ArrayList<String>();
		try {
			BufferedReader in = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = in.readLine()) != null) {
				strLines.add(line);
			}
			in.close();
			return strLines;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	/**
	 * ��������ļ�
	 * 
	 * @return
	 */
	public static String getRandomFileName() {
		StringBuffer buffer = new StringBuffer(
				"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
		StringBuffer sb = new StringBuffer();
		Random r = new Random();
		int range = buffer.length();
		for (int i = 0; i < 10; i++) {
			sb.append(buffer.charAt(r.nextInt(range)));
		}

		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmss");
		String strCreateTime = fmt.format(new Date().getTime()); // �ļ���������
		return strCreateTime + sb.toString();
	}

	/**
	 * �����ļ�
	 */
	public static void saveFile(InputStream stream, String filePath,
			String fineName) {
		OutputStream bos = null; // �ļ������
		try {

			File file = new File(filePath);
			if (!file.exists()) {
				file.mkdirs();
			}
			file = new File(filePath, fineName);
			bos = new FileOutputStream(file); // ͨ������������ļ�
			int bytesRead = 0; // �Ķ������ļ����ı�ʶ
			byte[] buffer = new byte[1024];
			// ��ȡ������
			while ((bytesRead = stream.read(buffer, 0, buffer.length)) != -1) {
				bos.write(buffer, 0, bytesRead);// ���ļ�д�������
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bos != null)
					bos.close();
				if (stream != null)
					stream.close();
			} catch (Exception e1) {
			}
		}
	}

	public static void saveFile(InputStream stream, String filePathName) {
		OutputStream bos = null; // �ļ������
		try {

			File file = new File(filePathName);
			if (!file.getParentFile().exists()) {
				file.mkdirs();
			}
			bos = new FileOutputStream(file); // ͨ������������ļ�
			int bytesRead = 0; // �Ķ������ļ����ı�ʶ
			byte[] buffer = new byte[1024];
			// ��ȡ������
			while ((bytesRead = stream.read(buffer, 0, buffer.length)) != -1) {
				bos.write(buffer, 0, bytesRead);// ���ļ�д�������
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bos != null)
					bos.close();
				if (stream != null)
					stream.close();
			} catch (Exception e1) {
			}
		}
	}

	public static void delFolder(String folderPath) {
		try {
			delAllFile(folderPath); // ɾ����������������
			String filePath = folderPath;
			filePath = filePath.toString();
			File myFilePath = new File(filePath);
			myFilePath.delete(); // ɾ����ļ���
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void delFile(String filePathName) {
		File file = new File(filePathName);
		boolean rs = file.delete();
		//System.out.println("ɾ�� " + rs + " : " + filePathName);
	}

	public static boolean delAllFile(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists()) {
			return flag;
		}
		if (!file.isDirectory()) {
			return flag;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);// ��ɾ���ļ���������ļ�
				delFolder(path + "/" + tempList[i]);// ��ɾ����ļ���
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * �����ļ�
	 * 
	 * @param destDir
	 * @param fileName
	 * @param extName
	 * @return
	 */
	public static File searchFile(String destDir, String fileName) {
		File parentFile = new File(destDir);
		File files[] = parentFile.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				File tempFile = searchFile(file.getPath(), fileName);
				if (tempFile != null) {
					return tempFile;
				}
				continue;
			}
			if (fileName.indexOf(".") > 0) {
				if (file.getName().equals(fileName)) {
					return file;
				}
			} else {
				if (file.getName()
						.substring(0, file.getName().lastIndexOf(".")).equals(
								fileName)) {
					return file;
				}
			}
		}
		return null;
	}

	/**
	 * �����ļ��������ļ�
	 * 
	 * @param fileDir
	 * @return
	 */
	public static ArrayList<File> findAllFiles(String fileDir) {
		File file = new File(fileDir);
		return findAllFiles(file);
	}

	/**
	 * ���Ҳ����ļ��������ļ�
	 * 
	 * @param fileDir
	 * @return
	 */
	public static ArrayList<File> findAllFiles(File fileDir) {
		File files[] = fileDir.listFiles();
		ArrayList<File> fileList = new ArrayList<File>();
		for (File file : files) {
			if (file.isDirectory()) {
				fileList.addAll(findAllFiles(file));
				continue;
			}
			fileList.add(file);
		}
		return fileList;
	}

	/**
	 * �����ļ���ָ����Ŀ¼����,�ᶨ�ļ���
	 * 
	 * @param srcFile
	 * @param destDirect
	 */
	public static void copyFile(File srcFile, File destDirect, String fileName) {
		try {
			if (!destDirect.exists()) {
				destDirect.mkdirs();
			}
			File destFile = new File(destDirect, fileName);
			FileInputStream in = new FileInputStream(srcFile);
			FileOutputStream out = new FileOutputStream(destFile);
			byte[] readByes = new byte[1024];
			while (in.read(readByes) != -1) {
				out.write(readByes);
			}
			out.close();
			in.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static String getExtName(String fileName){
		return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
	}
	public static String getClassDir(){
		return FileUtil.class.getResource("/").getPath();
	}
	public static String getClassPath(){
		return FileUtil.class.getResource("").getPath();
	}
	public static void main(String args[]){
		System.out.println(FileUtil.getClassDir());
	}
}
