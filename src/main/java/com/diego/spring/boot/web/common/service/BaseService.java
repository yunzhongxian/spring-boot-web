package com.diego.spring.boot.web.common.service;

import com.diego.spring.boot.web.common.utils.RtnResult;

/**
 * @author Manfred
 *
 */
public interface BaseService<T> {
	/**
	 * ���id���ض���
	 * @param id
	 * @return
	 */
	public T get(int id);
	/**
	 * ��Ӷ���
	 * @param t
	 */
	public RtnResult add(T t);
	/**
	 * ���¶���
	 * @param t
	 */
	public RtnResult update(T t);
	/**
	 * ���idɾ�����
	 * @param id
	 */
	public RtnResult delete(int id);
}
