package com.diego.spring.boot.web.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.diego.spring.boot.web.common.controller.BaseController;
import com.diego.spring.boot.web.common.utils.RtnResult;
import com.diego.spring.boot.web.system.service.UserService;

/**
 * @author Manfred
 *
 */
@Controller
@RequestMapping("/user")
public class UserContoller extends BaseController {
	@Autowired
	private UserService userService;

	@RequestMapping("/getUser")
	@ResponseBody
	public RtnResult getUser(int id){
		return RtnResult.ok(this.userService.get(id));
	}
}
