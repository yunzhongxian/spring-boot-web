package com.diego.spring.boot.web.common.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * yyyyMMddHHmmssSSS
 * @author manfred
 *
 */
public class DateUtils {
	
	/**
	 * 返回两个时间相差几分
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public static long diffMinutes(Date beginTime,Date endTime){
		return (endTime.getTime()-beginTime.getTime())/60000;
	}
	/**
	 * 返回两个时间相差秒数
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public static long diffSeconds(Date beginTime,Date endTime){
		return (endTime.getTime()-beginTime.getTime())/1000;
	}
	/**
	 * 返回两个时间相差几个月
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	public static int diffMonths(Date beginTime,Date endTime){
		Calendar bef = Calendar.getInstance();
		Calendar aft = Calendar.getInstance();
		bef.setTime(beginTime);
		aft.setTime(endTime);
		return aft.get(Calendar.MONTH) - bef.get(Calendar.MONTH)+(aft.get(Calendar.YEAR) - bef.get(Calendar.YEAR))*12;
		
	}
	/**
	 * 返回两个时间相差几天
	 * @param beginTime
	 * @param endTime
	 * @return endTime-beginTime
	 */
	public static int diffDays(Date beginTime,Date endTime){
		return (int)(endTime.getTime()/(1000*60*60*24)-beginTime.getTime()/(1000*60*60*24));
		
	}
	/**
	 * 按照指定格式返回指定时间的上
	 * @param returnPattern
	 * @return
	 */
	public static String getBeforeMonth(Date date,String returnPattern){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		return getDateString(calendar.getTime(),returnPattern);
	}
	/**
	 * 返回与指定时间间隔指定月数的时间
	 * @param date 指定时间
	 * @param months 间隔月数，正数返回指定时间之后的时间，负数返回与指定时间之前的时�?
	 * @param returnPattern 返回时间格式
	 * @return
	 */
	public static String getDiffMonth(Date date,int months,String returnPattern){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH,months);
		return getDateString(calendar.getTime(),returnPattern);
	}
	/**
	 * 返回与指定时间间隔指定天数的时间
	 * @param date 指定时间
	 * @param months 间隔月数，正数返回指定时间之后的时间，负数返回与指定时间之前的时�?
	 * @param returnPattern 返回时间格式
	 * @return
	 */
	public static String getDiffDay(Date date,int days,String returnPattern){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE,days);
		return getDateString(calendar.getTime(),returnPattern);
	}
	public static Date getDiffDayDate(Date date,int days){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE,days);
		return calendar.getTime();
	}
	public static Date getLastDateOfMonth(Date date)   {  
        Calendar cDay1   =   Calendar.getInstance();  
        cDay1.setTime(date);  
        int lastDay   =cDay1.getActualMaximum(Calendar.DAY_OF_MONTH);  
        Date lastDate = cDay1.getTime();  
        lastDate.setDate(lastDay);
        return lastDate;  
	} 
	public static int getLastDayOfMonth(Date date)   {  
        Calendar cDay1   =   Calendar.getInstance();  
        cDay1.setTime(date);  
        return cDay1.getActualMaximum(Calendar.DAY_OF_MONTH);      
	} 
	/**
	 * 按照指定格式返回指定天数的时�?,负数返回之前的时�?
	 * @param date
	 * @param days
	 * @param returnPattern
	 * @return
	 */
	public static String getBetweenDate(Date date,int days,String returnPattern){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, days);
		return getDateString(calendar.getTime(),returnPattern);
	}
	/**
	 * 根据Date转为指定格式String
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String getDateString(Date date,String pattern){
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	/**
	 * 将指定的时间格式转为Date对象
	 * @param stringDate
	 * @param pattern
	 * @return
	 */
	public static Date getDate(String stringDate,String pattern){
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = sdf.parse(stringDate);
		} catch (ParseException e) {
			throw new RuntimeException(e.getMessage());
		}
		return date;
	}
	
	public static Date[] getCurWeekDateRange(Date date){
		Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK)-2;
        if (w < 0){
            w = 0;
        }
        Date curDate[]=new Date[2];
		curDate[0]=getDiffDayDate(date,-w);
		curDate[1]=getDiffDayDate(date,6-w);
		return curDate;
	}	
	
	public static String[] getWeekDates(Date startTime,Date endTime){
		int days=diffDays(startTime,endTime)+1;
		String weekDates[]=new String[days];
		for(int i=0;i<days;i++){
			weekDates[i]=getWeekDate(getDateString(getDiffDayDate(startTime,i),"yyyy-MM-dd"),"yyyy-MM-dd");
		}
		return weekDates;
	}
	
	public static String getWeekDate(Date date){
		return getWeekDate(DateUtils.getDateString(date, "yyyy-MM-dd"),"yyyy-MM-dd");
	}
	
	public static String getWeekDate(String time, String format) {
		String strdate=getDateString(getDate(time,format),"yyyy-MM-dd");
		Calendar c = Calendar.getInstance(java.util.Locale.CHINA);
		String[] sp = strdate.split("-");
		c.set(Calendar.YEAR, Integer.parseInt(sp[0]));
		c.set(Calendar.MONTH, Integer.parseInt(sp[1]) - 1);
		c.set(Calendar.DATE, Integer.parseInt(sp[2]));

		int wd = c.get(Calendar.DAY_OF_WEEK);
		String x = "";
		switch (wd) {
		case 1:
			x = "星期日";
			break;
		case 2:
			x = "星期一";
			break;
		case 3:
			x = "星期二";
			break;
		case 4:
			x = "星期 三";
			break;
		case 5:
			x = "星期四";
			break;
		case 6:
			x = "星期五";
			break;
		case 7:
			x = "星期六";
			break;
		}
		return x;
	}
	
	/**
	 * 将如2011-01-12-2011-09-01有年月转成字符数组String[]{"201101","201102"..."201109"}
	 * @param starttime
	 * @param endtime
	 * @return
	 */
	public static String []getYearMonth(String starttime, String endtime){
		String startmonth = starttime.trim().replaceAll("-", "").substring(0, 6);
		String endmonth = endtime.trim().replaceAll("-", "").substring(0, 6);
		Date statdate = DateUtils.getDate(startmonth, "yyyyMM");
		Date enddate = DateUtils.getDate(endmonth, "yyyyMM");

		StringBuffer buf = new StringBuffer();
		Calendar c = Calendar.getInstance();
		while (statdate.before(enddate)||!statdate.after(enddate)) {
				buf.append(DateUtils.getDateString(statdate, "yyyyMM") + ",");
				c.setTime(statdate);
				c.add(Calendar.MONTH, 1);
				statdate = c.getTime();
		}
		int index=buf.lastIndexOf(",");
		if(index>=0){
			buf.deleteCharAt(index);
			return buf.toString().split(",");
		}
		return new String[]{};
	}
	public static String []getYearMonth(String starttime, String endtime,String returnPartern){
		String startmonth = starttime.trim().replaceAll("-", "").substring(0, 6);
		String endmonth = endtime.trim().replaceAll("-", "").substring(0, 6);
		Date statdate = DateUtils.getDate(startmonth, "yyyyMM");
		Date enddate = DateUtils.getDate(endmonth, "yyyyMM");

		StringBuffer buf = new StringBuffer();
		Calendar c = Calendar.getInstance();
		while (statdate.before(enddate)||!statdate.after(enddate)) {
				buf.append(DateUtils.getDateString(statdate, returnPartern) + ",");
				c.setTime(statdate);
				c.add(Calendar.MONTH, 1);
				statdate = c.getTime();
		}
		int index=buf.lastIndexOf(",");
		if(index>=0){
			buf.deleteCharAt(index);
			return buf.toString().split(",");
		}
		return new String[]{};
	}
	public static String[] getDays(Date startdate, Date enddate,
			String returnPattern) {
		StringBuffer buf = new StringBuffer();
		Calendar c = Calendar.getInstance();
		while (startdate.before(enddate) || !startdate.after(enddate)) {
			buf.append(DateUtils.getDateString(startdate, returnPattern) + ",");
			c.setTime(startdate);
			c.add(Calendar.DATE, 1);
			startdate = c.getTime();
		}
		int index = buf.lastIndexOf(",");
		if (index >= 0) {
			buf.deleteCharAt(index);
			return buf.toString().split(",");
		}
		return new String[] {};
	}
	public static void main(String args[]){
		Date startTime = (Date) DateUtils.getDate("2016-10-09", "yyyy-MM-dd");
		Date endTime = (Date) DateUtils.getDate((String)"2016-10-19", "yyyy-MM-dd");
		
		String weekdays[]=DateUtils.getWeekDates(startTime, endTime);
		
	
		for(int i=0;i<weekdays.length;i++){
			System.out.println(weekdays[i]);
		}
		
	}
}
