package com.diego.spring.boot.web.system.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.diego.spring.boot.web.common.utils.RtnResult;
import com.diego.spring.boot.web.system.dao.UserDao;
import com.diego.spring.boot.web.system.entity.User;
import com.diego.spring.boot.web.system.service.UserService;

/**
 * @author Manfred
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	public User get(int id) {
		return this.userDao.get(id);
	}

	
	public RtnResult add(User t) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public RtnResult update(User t) {
		// TODO Auto-generated method stub
		return null;
	}


	public RtnResult delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
