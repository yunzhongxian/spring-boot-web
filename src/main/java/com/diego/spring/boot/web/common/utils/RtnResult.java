package com.diego.spring.boot.web.common.utils;


/**
 * 
 * @author Manfred
 *
 */
public class RtnResult {
	
	public static final int SUCCESS=1;
	public static final int ERROR=0;
	
	private int status;//状�?
	private Object data;//返回对象
	private String text;//消息
	public RtnResult(){}
	public RtnResult(int status){
		this.status=status;
	}	
	public RtnResult(int status,String text){
		this.status=status;
		this.text=text;
	}
	public RtnResult(int status,String text,Object data){
		this.status=status;
		this.text=text;
		this.data=data;
	}
	public static RtnResult ok(Object data){
		return new RtnResult(RtnResult.SUCCESS,"成功",data);
	}
	public static RtnResult error(String message,Object data){
		return new RtnResult(RtnResult.ERROR,message,data);
	}
	public static RtnResult error(String message){
		return new RtnResult(RtnResult.ERROR,message,null);
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
