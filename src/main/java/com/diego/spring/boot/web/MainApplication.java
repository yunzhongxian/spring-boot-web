package com.diego.spring.boot.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author Manfred
 *
 */
@EnableAutoConfiguration
@SpringBootApplication
@MapperScan("com.diego.spring.boot.web.**.dao")
public class MainApplication  extends SpringBootServletInitializer{
	//启动类
	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);
    }

}

