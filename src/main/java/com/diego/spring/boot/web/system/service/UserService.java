package com.diego.spring.boot.web.system.service;

import com.diego.spring.boot.web.common.service.BaseService;
import com.diego.spring.boot.web.system.entity.User;

/**
 * @author Manfred
 *
 */
public interface UserService extends BaseService<User>{

}
