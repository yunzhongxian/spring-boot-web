package com.diego.spring.boot.web.common.utils;


/**
 * 自定义异常类
 */
public class CommonException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int status;
	
	private String text;

	private Object data;
	
	public CommonException(int status, String text) {
		this.status = status;
		this.text = text;
	}
	public CommonException(int status, String text, Object data) {
		this.status = status;
		this.text = text;
		this.data = data;
	}
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}

}
