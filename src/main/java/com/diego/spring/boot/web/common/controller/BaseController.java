package com.diego.spring.boot.web.common.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.diego.spring.boot.web.common.utils.CommonException;
import com.diego.spring.boot.web.common.utils.RtnResult;

/**
 * @author Manfred
 *
 */
public class BaseController {
	final static Logger logger = Logger.getLogger(BaseController.class);

	@ExceptionHandler
	public @ResponseBody RtnResult exp(HttpServletRequest request, Exception ex) {
		if (ex instanceof CommonException) {
			logger.error(((CommonException) ex).getText());
			return new RtnResult(((CommonException) ex).getStatus(), ((CommonException) ex).getText(),
					((CommonException) ex).getData());
		}
		logger.error("", ex);
		return new RtnResult(RtnResult.ERROR, ex.getMessage());
	}

	public final static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");

			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_CLIENT_IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_X_FORWARDED_FOR");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}
		} else if (ip.length() > 15) {
			String[] ips = ip.split(",");
			for (int index = 0; index < ips.length; index++) {
				String strIp = (String) ips[index];
				if (!("unknown".equalsIgnoreCase(strIp))) {
					ip = strIp;
					break;
				}
			}
		}
		return ip;
	}
}
