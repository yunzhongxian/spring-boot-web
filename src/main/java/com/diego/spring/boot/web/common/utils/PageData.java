package com.diego.spring.boot.web.common.utils;


import java.util.List;

public class PageData<T> {
	private List<T> data;
	private int count;
	private int page;
	private int limit;
	
	private int start;
	private int end;
	
	private int totalPage;
	
	private int pageNum=5;
	
	public PageData(){}
	public PageData(List<T> data, int count, int page, int limit) {
		super();
		this.data = data;
		this.count = count;
		this.page = page;
		this.limit = limit;
		this.totalPage=count/limit;
		if(count%limit>0){
			totalPage++;
		}
		if(page%pageNum==0){
			end=page+pageNum>=totalPage?totalPage:page+pageNum;
		}else{
			int tempEnd=page+pageNum-page%pageNum;
			if(tempEnd>=totalPage){
				end=totalPage;
			}else{
				end=tempEnd;
			}
		}
		start=end-pageNum<=0?1:end-pageNum;
	}
	
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	
}
