package com.diego.spring.boot.web.system.dao;

import com.diego.spring.boot.web.common.dao.BaseDao;
import com.diego.spring.boot.web.system.entity.User;

/**
 * @author Manfred
 *
 */
public interface UserDao extends BaseDao<User>{

}
